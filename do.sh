#!/bin/bash


case $1 in
	shell)
		docker-compose exec postgresql psql -U mail
	;;
	dump)
		docker-compose exec postgresql pg_dump -U mail mail
	;;
	bash)
		docker-compose exec postfix /bin/bash
	;;
	testmail)
		docker-compose exec postfix /bin/bash -c 'echo "Email body text" | mail -s "Email subject line" alias@mcr.robjh.com'
	;;
	pw)
		docker-compose exec postfix /bin/bash -c "doveadm pw -s SHA512-CRYPT -p $2"
	;;
	sqltest)
		echo postmap -q example.com pgsql:/etc/postfix/virtual-mailbox-domains.cf
		docker-compose exec postfix postmap -q example.com pgsql:/etc/postfix/virtual-mailbox-domains.cf
		echo postmap -q manchester.robjh.com pgsql:/etc/postfix/virtual-mailbox-domains.cf
		docker-compose exec postfix postmap -q manchester.robjh.com pgsql:/etc/postfix/virtual-mailbox-domains.cf

		echo postmap -q testing@manchester.robjh.com pgsql:/etc/postfix/virtual-mailbox-maps.cf
		docker-compose exec postfix postmap -q testing@manchester.robjh.com pgsql:/etc/postfix/virtual-mailbox-maps.cf

		echo postmap -q alias@manchester.robjh.com pgsql:/etc/postfix/virtual-alias-maps.cf
		docker-compose exec postfix postmap -q alias@manchester.robjh.com pgsql:/etc/postfix/virtual-alias-maps.cf


	;;
	*)
		echo usage\; $0 shell
	;;
esac
