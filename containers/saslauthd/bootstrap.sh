#!/bin/bash

cat << EOF > /etc/pam_pgsql.conf
debug
connect =\
	host=${PGSQL_HOST}\
	dbname=${PGSQL_NAME}\
	user=${PGSQL_USER}\
	password=${PGSQL_PASS}\
	connect_timeout=15
pw_type =\
	crypt_sha512
auth_query =\
	SELECT ${PGSQL_TBLPREFIX}userauth.password as password \
	FROM ${PGSQL_TBLPREFIX}user \
	INNER JOIN ${PGSQL_TBLPREFIX}domain ON ${PGSQL_TBLPREFIX}user.id_domain=${PGSQL_TBLPREFIX}domain.id \
	INNER JOIN ${PGSQL_TBLPREFIX}userauth ON ${PGSQL_TBLPREFIX}user.id=${PGSQL_TBLPREFIX}userauth.id_user \
	WHERE ${PGSQL_TBLPREFIX}user.name = SPLIT_PART(%u, '@', 1) AND ${PGSQL_TBLPREFIX}user.enabled=true \
	AND ${PGSQL_TBLPREFIX}domain.name = SPLIT_PART(%u, '@', 2) AND ${PGSQL_TBLPREFIX}domain.enabled=true\
	LIMIT 1;
EOF

/usr/sbin/saslauthd "$@"
