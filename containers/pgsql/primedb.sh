#!/bin/bash

pfx=${POSTGRES_TABLE_PREFIX}
schema_v=1

function setup_database {
	cat <<- EOF
	SET statement_timeout = 0;
	SET lock_timeout = 0;
	SET idle_in_transaction_session_timeout = 0;
	SET client_encoding = 'UTF8';
	SET standard_conforming_strings = on;
	SELECT pg_catalog.set_config('search_path', '', false);
	SET check_function_bodies = false;
	SET xmloption = content;
	SET client_min_messages = warning;
	SET row_security = off;

	--
	-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner:
	--

	CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


	--
	-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner:
	--

	COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


	SET default_tablespace = '';

	SET default_with_oids = false;
	EOF
}

function alter_table {
	cat <<- EOF
	ALTER TABLE public.${pfx}${1} OWNER TO ${POSTGRES_USER};

	--
	-- Name: ${pfx}${1}_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
	--

	CREATE SEQUENCE public.${pfx}${1}_id_seq
		AS integer
		START WITH 1
		INCREMENT BY 1
		NO MINVALUE
		NO MAXVALUE
		CACHE 1;

	ALTER TABLE public.${pfx}${1}_id_seq OWNER TO ${POSTGRES_USER};

	--
	-- Name: ${pfx}${1}_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
	--

	ALTER SEQUENCE public.${pfx}${1}_id_seq OWNED BY public.${pfx}${1}.id;

	ALTER TABLE ONLY public.${pfx}${1} ALTER COLUMN id SET DEFAULT nextval('public.${pfx}${1}_id_seq'::regclass);

	EOF
}

function create_tables {
	cat <<- EOF
	--
	-- Name: ${pfx}alias; Type: TABLE; Schema: public; Owner: postgres
	--

	CREATE TABLE public.${pfx}alias (
		id integer NOT NULL,
		hash bytea NOT NULL,
		id_user integer NOT NULL,
		address text,
		enabled boolean DEFAULT true
	);

	`alter_table "alias"`


	--
	-- Name: ${pfx}domain; Type: TABLE; Schema: public; Owner: postgres
	--

	CREATE TABLE public.${pfx}domain (
		id integer NOT NULL,
		name character varying(50) NOT NULL,
		enabled boolean DEFAULT true
	);

	`alter_table "domain"`


	--
	-- Name: ${pfx}user; Type: TABLE; Schema: public; Owner: postgres
	--

	CREATE TABLE public.${pfx}user (
		id integer NOT NULL,
		name character varying(50) NOT NULL,
		id_domain integer NOT NULL,
		id_transport integer DEFAULT 0,
		enabled boolean DEFAULT true
	);

	`alter_table "user"`


	CREATE TABLE public.${pfx}userauth (
		id integer NOT NULL,
		id_user integer NOT NULL,
		password character varying(150) NOT NULL
	);

	`alter_table "userauth"`


	--
	-- Name: ${pfx}transport; Type: TABLE; Schema: public; Owner: postgres
	--

	CREATE TABLE public.${pfx}transport (
		id integer NOT NULL,
		destination character varying(127) NOT NULL
	);

	`alter_table "transport"`


	--
	-- Name: ${pfx}update; Type: TABLE; Schema: public; Owner: postgres
	--

	CREATE TABLE public.${pfx}update (
		id integer NOT NULL,
		schema_version integer NOT NULL,
		applied timestamptz DEFAULT now()
	);

	`alter_table "update"`

	EOF
}

function fix_perms {
	cat <<- EOF
	--
	-- Name: ${pfx}${1}_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
	--

	SELECT pg_catalog.setval('public.${pfx}${1}_id_seq', 1, true);

	--
	-- Name: ${pfx}${1} ${pfx}${1}_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
	--

	ALTER TABLE ONLY public.${pfx}${1}
		ADD CONSTRAINT ${pfx}${1}_pkey PRIMARY KEY (id);

	--
	-- Name: TABLE ${pfx}${1}; Type: ACL; Schema: public; Owner: postgres
	--

	GRANT SELECT ON TABLE public.${pfx}${1} TO mail;

	EOF
}

function populate_tables {

	cat <<- EOF
	DO \$\$
	DECLARE lastid bigint;
	BEGIN
	EOF

	cat <<- EOF
	INSERT INTO public.${pfx}update (schema_version)
	VALUES ('${schema_v}');
	EOF


	if [ ! -z "${DEFAULT_DOMAIN}" ]; then
		cat <<- EOF
		INSERT INTO public.${pfx}domain (name, enabled)
		VALUES ('${DEFAULT_DOMAIN}', true)
		RETURNING id INTO lastid;
		EOF

		if [ ! -z "${TESTING_ACCOUNT}" ]; then
		cat <<- EOF
			INSERT INTO public.${pfx}user (name, id_domain, enabled)
			VALUES ('testing', lastid, true)
			RETURNING id INTO lastid;

			-- password is 'testing'.
			INSERT INTO public.${pfx}userauth(id_user, password)
			VALUES (lastid, '\$6\$rounds=656000\$bPGIgGIMjjYWoEZh\$gKJ/UwBv85Ny85C3vbSKaCUbCZyJJhgYRQjDuRff.Kt.I0YLTJvEZDvkw.x9g.KDGPwiHqRtc0RUnddUg1jYI0');

			INSERT INTO public.${pfx}alias (hash, id_user, address, enabled)
			VALUES (SHA512('alias@${DEFAULT_DOMAIN}'), lastid, '', true);
			EOF
		fi
	fi


	echo END \$\$;
}

psql -U ${POSTGRES_USER} -t <<- EOF
\connect mail

`setup_database`
`create_tables`
`fix_perms "domain"`
`fix_perms "user"`
`fix_perms "alias"`
`populate_tables`

EOF
