import psycopg2
import hashlib
import passlib.hash

class Mail_Database:
	def __init__(self, dbname, username, password, hostname='127.0.0.1', prefix=''):
		self.connection = None
		self.host   = hostname
		self.user   = username
		self.passwd = password
		self.dbname = dbname
		self.prefix = prefix

	def __enter__(self):
		self.connect()
		return self

	def __exit__(self, type, value, traceback):
		self.disconnect()

	def connect(self):
		self.connection = psycopg2.connect(
			host     = self.host,
			database = self.dbname,
			user     = self.user,
			password = self.passwd
		)

	def disconnect(self):
		if self.connection is not None:
			self.connection.close()
			self.connection = None

	class Cursor:
		def __init__(self, connection, autocommit=False):
			self.cur = connection.cursor()
			self.con = connection
			self.autocommit = autocommit
		def __enter__(self):
			return self.cur
		def __exit__(self, type, value, traceback):
			if self.autocommit:
				self.con.commit()
			self.cur.close()

	def test(self):
		with self.Cursor(self.connection) as cur:
			cur.execute('SELECT version()')
			db_version = cur.fetchone()
		print('PostgreSQL database version: {}'.format(db_version))

	def tuple_to_dict(self, data, key):
		ret = {}
		for i,k in enumerate(key):
			ret[k] = data[i]
		return ret

	def _generic_fetch(self, key, table, id=0):
		where = ''
		if id:
			where = ' WHERE id = %s'
		sql = 'SELECT {key} FROM {pfx}{table}{where};'.format(
			key = ', '.join(key),
			pfx = self.prefix,
			table = table,
			where = where
		)
		with self.Cursor(self.connection) as cur:
			cur.execute(sql, (id,))
			rows = cur.fetchall()
		ret = []
		for r in rows:
			ret.append(self.tuple_to_dict(r, key))
		return ret
	def domain_fetch(self, id=0):
		return self._generic_fetch(('id', 'name', 'enabled'), 'domain', id=id)
	def user_fetch(self, id=0):
		return self._generic_fetch(('id', 'name', 'id_domain', 'enabled'), 'user', id=id)
	def alias_fetch(self, id=0):
		ret = self._generic_fetch(('id', 'hash', 'id_user', 'address', 'enabled'), 'alias', id=id)
		for a in ret:
			a['hash'] = a['hash'].tobytes()
		return ret

	def domain_add(self, name, enabled=True):
		sql = 'INSERT INTO {pfx}domain (name, enabled) VALUES (%s, %s) RETURNING id;'.format(pfx=self.prefix)
		with self.Cursor(self.connection, autocommit=True) as cur:
			cur.execute(sql, (name, enabled))
			result = cur.fetchone()
			return result[0]

	def user_add(self, name, id_domain, enabled=True):
		sql = 'INSERT INTO {pfx}user (name, id_domain, enabled) VALUES (%s, %s, %s) RETURNING id;'.format(pfx=self.prefix)
		with self.Cursor(self.connection, autocommit=True) as cur:
			cur.execute(sql, (name, id_domain, enabled))
			result = cur.fetchone()
			return result[0]

	def user_password_add(self, id_user, password):
		sql = 'INSERT INTO {pfx}userauth (id_user, password) VALUES (%s, %s) RETURNING id;'.format(pfx=self.prefix)
		crypt = passlib.hash.sha512_crypt
		password_sha = crypt.hash(password);
		with self.Cursor(self.connection, autocommit=True) as cur:
			cur.execute(sql, (id_user, password_sha))
			result = cur.fetchone()
			return result[0]

	def alias_add(self, address, id_user, address_encrypted='', enabled=True):
		sql = 'INSERT INTO {pfx}alias (hash, address, id_user, enabled) VALUES (%s, %s, %s, %s) RETURNING id;'.format(pfx=self.prefix)
		addr_hash = hashlib.sha512()
		addr_hash.update(address.encode())
		with self.Cursor(self.connection, autocommit=True) as cur:
			cur.execute(sql, (addr_hash.digest(), address_encrypted, id_user, enabled))
			result = cur.fetchone()
			return result[0]

	def domain_by_name(self, name):
		sql = 'SELECT id FROM {pfx}domain WHERE name = %s LIMIT 1'.format(pfx=self.prefix)
		with self.Cursor(self.connection) as cur:
			cur.execute(sql, (name,))
			result = cur.fetchone()
			return result[0] if result else None

	def user_by_name(self, local, domain):
		sql = '''
			SELECT {pfx}user.id FROM {pfx}user
			INNER JOIN {pfx}domain ON {pfx}user.id_domain = {pfx}domain.id
			WHERE {pfx}user.name = %s AND {pfx}domain.name = %s
			LIMIT 1
		'''.format(pfx=self.prefix)
		with self.Cursor(self.connection) as cur:
			cur.execute(sql, (local, domain))
			result = cur.fetchone()
			return result[0] if result else None
