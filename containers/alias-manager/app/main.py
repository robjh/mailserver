#!/usr/bin/env python3

from pretty_bad_protocol import gnupg
import os
import argparse
import alias_manager

def get_key(gpg, keyuid):
	all_keys = gpg.list_keys()
	for k in all_keys:
		if keyuid in k['uids']:
			return k
	return None


def parse_args():
	parser = argparse.ArgumentParser(description='Manipulate values in the Mail Database.')

	parser.add_argument('--exit', dest='exit', action='store_true')
	parser.set_defaults(exit=False)

	parser.add_argument('--add-domain', nargs='*')
	parser.add_argument('--add-user', nargs='*')
	parser.add_argument('--add-alias', nargs='*')

	return parser.parse_args()

def add_domain(db, domain):
	# TODO: Look for duplicate domains.
	#       Sanitise inputs with regex.
	db.domain_add(domain)
	print(domain)

def add_user(db, user):
	# TODO: Sanitise inputs with regex.
	#       Look for duplicate users.
	#       Add a mechanism to generate a new random password.
	local, at, domain = user.rpartition('@')
	domain_id = db.domain_by_name(domain)
	if domain_id == None:
		print("No such domain: {}".format(domain))
	else:
		user_id = db.user_add(local, domain_id)
		passwd = input("Password for '{}': ".format(user))
		if len(passwd):
			db.user_password_add(user_id, passwd)
			print("{} (with auth)".format(user))
		else:
			print(user)

def add_alias(db, alias, alias_encrypted, user_id):
	# TODO: Make sure the alias' domain exists.
	#       Look for duplicate aliases
	addr = db.alias_add(alias, user_id, alias_encrypted)

def main():
	args = parse_args()

	if args.exit:
		return 0

	db_user=os.environ['PGSQL_USER']
	db_pass=os.environ['PGSQL_PASS']
	db_host=os.environ['PGSQL_HOST']
	db_name=os.environ['PGSQL_NAME']
	db_tblprefix=os.environ['PGSQL_TBLPREFIX']

	with alias_manager.Mail_Database(db_name, db_user, db_pass, db_host, db_tblprefix) as db:

		if args.add_domain:
			for domain in args.add_domain:
				add_domain(db, domain)

		if args.add_user:
			for user in args.add_user:
				add_user(db, user)

		if args.add_alias:
			# TODO: Switch to exception based error checking;
			#           Unravel this mess.
			#       Optionally specify user by local part only.

			if len(args.add_alias) == 1:
				print("Please specify a user as the final argument.")
			else:
				user = args.add_alias.pop()
				user_local, at, user_domain = user.rpartition('@')
				user_id = db.user_by_name(user_local, user_domain)
				if user_id == None:
					print("No such user: {}".format(user))
				else:
					# setup GPG, get our public key.
					# if we're running in a fresh container the key will have to be imported.
					gpg = gnupg.GPG()
					maildbpub = get_key(gpg, 'maildb')
					if maildbpub == None:
						with open('/app/maildb.pub', 'r') as keyfile:
							gpg.import_keys(keyfile.read())
							maildbpub = get_key(gpg, 'maildb')

					for alias in args.add_alias:
						add_alias(db, alias, str(gpg.encrypt(alias.encode(), maildbpub['fingerprint'])), user_id)
						print('{} -> {}'.format(alias, user))

	return 0

if __name__ == "__main__":
	main()

